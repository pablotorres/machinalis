"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase


class TestIndexView(TestCase):
    fixtures = ['polls_views_testdata.json']

    def test_good_url(self):
        """
        Tests index view.
        """
        resp = self.client.get('/polls/')
        self.assertEqual(resp.status_code, 200)

    def test_check_context(self):
        resp = self.client.get('/polls/')
        self.assertTrue('poll_list' in resp.context)

    def test_detail(self):
        """
        Test detail view.
        """
        # Test bad url.
        response = self.client.get('/polls/10/')
        self.assertEqual(response.status_code, 404)

        # Test good url.
        response = self.client.get('/polls/1/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('poll' in response.context)
        self.assertTrue('form' in response.context)
