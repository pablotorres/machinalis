from django.conf.urls import patterns, url

urlpatterns = patterns('polls.views',
    url(r'^$', 'index', name='index'),
    url(r'^(?P<poll_id>\d+)/$', 'detail', name='detail'),
    url(r'^(?P<poll_id>\d+)/results/$', 'results', name='results'),
    url(r'^(?P<poll_id>\d+)/vote/$', 'vote', name='vote'),
    url(r'^most-voted/$', 'most_voted', name='most_voted'),
    url(r'^ranking/$', 'ranking', name='ranking'),
    url(r'^poll-create/$', 'poll_create', name='poll_create'),
    url(r'^(?P<poll_id>\d+)/edit-poll/$', 'edit_poll', name='edit_poll'),
)
