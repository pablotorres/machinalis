from polls.models import Poll, Choice
from django.contrib import admin

admin.site.register(Poll, PollAdmin)
admin.site.register(Choice)
