# Forms for bootstrap.
from django import forms
from polls.models import Choice, Poll
from django.forms.models import inlineformset_factory
from django.forms.formsets import BaseFormSet


class VoteForm(forms.Form):
    choice = forms.ModelChoiceField(queryset=Choice.objects.all(),
        widget=forms.RadioSelect(),
        empty_label=None,
        label='',)

    def __init__(self, *args, **kwargs):
        poll = kwargs.pop('poll')
        super(VoteForm, self).__init__(*args, **kwargs)
        self.fields['choice'].queryset = poll.choice_set.all()


ChoiceFormSet = inlineformset_factory(Poll,
    Choice,
    can_delete=False,
    extra=3,
    exclude=('votes',),
    can_order=True,
    )


class PollForm(forms.ModelForm):
    class Meta:
        model = Poll
        exclude = ('pub_date',)


class EditPollForm(forms.Form):
    question = forms.CharField(max_length=200, label="Titulo de encuesta")
