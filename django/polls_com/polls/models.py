import datetime
from django.db import models
from django.utils import timezone
from django.db.models import Sum


class Poll(models.Model):
    question = models.CharField(max_length=200, unique=True)
    pub_date = models.DateTimeField('date published', default=timezone.now())

    class Meta:
        permissions = (
            ('can_vote', ('puede votar')),
            ('can_create', ('puede crear')),
            ('can_edit', ('puede editar')),
        )

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

    def __unicode__(self):
        return self.question


class Choice(models.Model):
    poll = models.ForeignKey(Poll)
    choice = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    class Meta:
        order_with_respect_to = 'poll'

    def porcentual(self):
        result = 0
        total_votes = self.poll.choice_set.all().aggregate(total=Sum('votes'))['total']
        if total_votes:
            result = ((self.votes + 0.0) / total_votes) * 100
        return result

    def __unicode__(self):
        return self.choice
