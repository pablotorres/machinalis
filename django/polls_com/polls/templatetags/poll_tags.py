from django import template

HTML = '<div class="progress progress-striped active"><div class="bar" style="width: {0}%;"></div></div>'

register = template.Library()


@register.simple_tag
def porcentual_tag(choice):
	p = choice.porcentual()
	return HTML.format(p)
