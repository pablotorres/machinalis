from django.shortcuts import render, get_object_or_404, redirect, render_to_response
from polls.models import Poll, Choice
from django.db.models import Sum
from django.views.generic import FormView, ListView, DetailView, ArchiveIndexView
from forms import VoteForm, PollForm, ChoiceFormSet, EditPollForm
from django.template import RequestContext
from django.forms.models import inlineformset_factory
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def poll_create(request):
    if request.POST:
        form = PollForm(request.POST)
        formset = ChoiceFormSet(request.POST, instance=Poll())
        if form.is_valid():
            poll = form.save(commit=False)
            formset.instance = poll
            if formset.is_valid():
                if formset.save(commit=False) == []:
                    message = "Se debe colocal al menos una opcion!"
                    return render_to_response("polls/poll-create.html", {
                        "form": form,
                        "formset": formset,
                        "message": message,
                    }, context_instance=RequestContext(request))
                poll.save()
                formset.save()
                order_list = []
                for choice_form in formset.ordered_forms:
                    c = choice_form.save(commit=False)
                    order_list.append(c.id)
                poll.set_choice_order(order_list)
                return render(request, 'polls/success.html')
    else:
        formset = ChoiceFormSet(instance=Poll())
        form = PollForm()
    return render_to_response("polls/poll-create.html", {
        "form": form,
        "formset": formset,
    }, context_instance=RequestContext(request))


def edit_poll(request, poll_id):
    FormSet = inlineformset_factory(Poll,
            Choice,
            can_delete=True,
            extra=1,
            can_order=True,
            exclude=('votes',),)
    poll = get_object_or_404(Poll, pk=poll_id)
    if request.POST:
        form = EditPollForm(request.POST)
        formset = FormSet(request.POST, instance=poll)
        if form.is_valid() and form.has_changed():
            poll.question = form.cleaned_data['question']
            poll.save()
        if formset.is_valid():
            formset.instance = poll
            formset.save()
            order_list = []
            for choice_form in formset.ordered_forms:
                order_list.append(choice_form.save(commit=False).id)
            poll.set_choice_order(order_list)
            return render(request, 'polls/success.html')
    else:
        form = EditPollForm()
        form.fields['question'].initial = poll.question
        formset = FormSet(instance=poll)
    return render_to_response("polls/edit-poll.html", {
        "poll": poll,
        "form": form,
        "formset": formset,
    }, context_instance=RequestContext(request))


def index(request, page=None):
    polls = Poll.objects.all().order_by('-pub_date')
    paginator = Paginator(polls, 2)

    page = request.GET.get('page')
    try:
        poll_list = paginator.page(page)
    except PageNotAnInteger:
        poll_list = paginator.page(1)
    except EmptyPage:
        poll_list = paginator.page(paginator.num_pages)

    return render_to_response('polls/index.html', {"poll_list": poll_list, 'request': request})


# index = ArchiveIndexView.as_view(
#     date_field='pub_date',
#     paginate_by=10,
#     allow_empty=True,
#     queryset=Paginator(Poll.objects.all().order_by('-pub_date'), 10),
#     template_name='polls/index.html',
# )

# class IndexView(ArchiveIndexView):
#     date_field = 'pub_date'
#     paginate_by = 10
#     allow_empty = True
#     queryset = Poll.objects.all().order_by('-pub_date')
#     template_name = 'polls/index.html'

#     def get_context_data(self, **kwargs):
#         context = super(IndexView, self).get_context_data(**kwargs)
#         context['latest'] = Paginator(context['latest'], 10).page(1)
#         return context
#         # if 'page' in self.kwargs:
#         #     context['latest'] = Paginator(context['latest'])
#         # else:


# index = IndexView.as_view()


class DetailViews(DetailView):
    model = Poll
    pk_url_kwarg = 'poll_id'
    template_name = 'polls/detail.html'

    def get_context_data(self, **kwargs):
        context = super(DetailViews, self).get_context_data(**kwargs)
        poll = get_object_or_404(Poll, pk=self.kwargs['poll_id'])
        context['form'] = VoteForm(poll=poll)
        return context

detail = DetailViews.as_view()


class VoteView(FormView):
    form_class = VoteForm
    http_method_names = ['post']

    def get_form(self, form_class):
        self.poll = get_object_or_404(Poll, pk=self.kwargs['poll_id'])
        return VoteForm(self.request.POST, poll=self.poll)

    def form_valid(self, form):
        choice = form.cleaned_data['choice']
        choice.votes += 1
        choice.save()
        return redirect('polls:results', poll_id=self.poll.pk)

    def form_invalid(self, form):
        return render(self.request, 'polls/detail.html', {'poll': self.poll, 'form': form})

vote = VoteView.as_view()


class ResultView(DetailView):
    model = Poll
    pk_url_kwarg = 'poll_id'
    template_name = 'polls/result.html'

    def get_context_data(self, **kwargs):
        context = super(ResultView, self).get_context_data(**kwargs)
        poll = get_object_or_404(Poll, pk=self.kwargs['poll_id'])
        context['poll'] = poll
        return context

results = ResultView.as_view()


class RankingView(ListView):
    template_name = 'polls/ranking.html'
    queryset = Poll.objects.all()

    def get_context_data(self, **kwargs):
        context = super(RankingView, self).get_context_data(**kwargs)
        polls = Poll.objects.annotate(votes=Sum('choice__votes')).order_by('-votes')
        context['poll_list'] = polls
        return context

ranking = RankingView.as_view()


class MostVotedView(RankingView):
    template_name = 'polls/most-voted.html'

most_voted = MostVotedView.as_view()
