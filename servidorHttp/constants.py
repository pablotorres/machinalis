#!/usr/bin/python
# -*- coding: utf-8 -*-
# All constants

# Default port
PORT = 8000

# Templates names
INDEX_TEMPLATE = 'index_template.html'
ACCEPT_TEMPLATE = "accept_template.html"

FILE_DB = "file.txt"

# Constants for all error
PAGE_NOT_FOUND = "<h1> Error 404 </h1>\n"
INTERNAL_ERROR = "<h1> Ups! Internal Error 500</h1>"

# urls
INDEX_PAGE = '/'
SUCCESS_PAGE = '/accept'
DATABASE_PAGE = '/database'

#headers
HEADER_CONTENT_TYPE = {'content-Type':'text/html; charset=utf-8'}
HEADER_CONNECTION = {'Connection': 'Keep-Alive'}

# Text for database
TEXT_DATABASE = "--------------------------------------------<br/>" + \
			'name = {0}<br/>lastname=¸{1}<br/>address = {2}<br/>'