import optparse
import myserver

from constants import *

def main():
	# Se toman los argumentos por consola.
	parser = optparse.OptionParser(usage="%prog [options]")

	parser.add_option("-f","--database", help=u"File to save your datas.", default=FILE_DB)
	parser.add_option("-p","--port", help=u"TCP port number used to listen", default=PORT)

	options, args = parser.parse_args()

	port = int(options.port)
	database = options.database
	print 'Started httpserver on port ' , port
	print 'For disconect to server press CTRL+C '
	myserver.run(port, database)

if __name__ == '__main__':
	main()
