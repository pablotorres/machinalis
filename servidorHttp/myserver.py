from SimpleHTTPServer import SimpleHTTPRequestHandler 

import os
import SocketServer
import urllib2
import cgi
import time

from constants import *


def run(port, database):
    """
    Run server, for disconnect for server
    press CTRL+C.
    """
    try:
        server = SocketServer.TCPServer(('', port), MyHandler)
        MyHandler.database = database
        server.serve_forever()

    except KeyboardInterrupt:
        print '\n^C received, shutting down the web server'
        server.socket.close()

def _get_page(templ):
    page = ''
    try:
        f = open(templ, 'r')
        page = f.read()
        f.close()
    except AttributeError:
        page = ERROR
    return page

def _write_file(dic, file_db):
    f = open(file_db, 'w')
    f.seek(0, 2)
    text = TEXT_DATABASE.format(dic['name'].value, 
                    dic['lastname'].value, dic['address'].value)
    f.write(text)
    f.flush()
    f.close()

def _get_last_modified_time():
    try:
        s = os.path.getmtime(FILE_DB)
        t = time.ctime(s)
        parsed = time.strptime(t)
    except OSError:
        raise Error500

    return time.strftime("%a, %d %b %Y %H:%M:%S", parsed) + ' GMT'

def _less_than(time_compare, last_modified):
    return (time.strptime(time_compare[:-4], "%a, %d %b %Y %H:%M:%S") < time.strptime(last_modified[:-4], "%a, %d %b %Y %H:%M:%S"))


class MyHandler(SimpleHTTPRequestHandler):
    database = None

    #Handler for the GET requests
    def do_GET(self):
        # print "-----------------------------GET.-------------------------------"
        # for property, value in vars(self).iteritems():
        #     print property, ": ", value
        url = urllib2.unquote(self.path)
        if url == INDEX_PAGE:
            self.index_page()
        elif url == SUCCESS_PAGE:
            self.accept_form()
        elif url == DATABASE_PAGE:
            self.download_file()

    #Handler for the POST requests
    def do_POST(self):
        # print "-----------------------------POST.-------------------------------"
        # for property, value in vars(self).iteritems():
        #     print property, ": ", value
        url = urllib2.unquote(self.path)
        if url == '/':
            form = cgi.FieldStorage(
                fp=self.rfile, 
                headers=self.headers,
                environ={'REQUEST_METHOD':'POST',
                         'CONTENT_TYPE':self.headers['content-Type'],
                         })
            if all((k in form and form[k].value != '') for k in ('name', 'address', 'lastname')):
                _write_file(form, self.database)
                header = HEADER_CONTENT_TYPE
                header.update({'Location': SUCCESS_PAGE})
                self.send_req(302, header)
            else:
                name = ''
                lastname = ''
                address = ''
                if form.has_key('name'):
                    name = form['name'].value
                if form.has_key('address'):
                    address = form['address'].value
                if form.has_key('lastname'):
                    lastname = form['lastname'].value
                self.index_page('Todos los campos son OBLIGATORIOS', name, lastname, address)


    def index_page(self, message='', name='', lastname='', address=''):
        page = _get_page(INDEX_TEMPLATE)
        page = page.format(message, name, lastname, address)
        self.send_req(200, HEADER_CONTENT_TYPE)
        self.wfile.write(page)

    def accept_form(self):
        page = _get_page(ACCEPT_TEMPLATE)
        self.send_req(200, HEADER_CONTENT_TYPE)
        self.wfile.write(page)

    def download_file(self):
        last_modified = ''
        header = HEADER_CONTENT_TYPE
        if 'if-modified-since' in self.headers:
            t = self.headers['if-modified-since']
            try:
                last_modified = _get_last_modified_time()
            except Error500:
                self.send_req(500, HEADER_CONTENT_TYPE)
                self.wfile.write(INTERNAL_ERROR)
                return
            if not _less_than(t, last_modified):
                last_modified = _get_last_modified_time()
                header.update({'Last-Modified': last_modified})
                #self.send_req(304, header)
                self.send_response(304)
                return
        last_modified = _get_last_modified_time()
        header.update({'Last-Modified': last_modified})
        self.send_req(200, header)
        page = _get_page(FILE_DB)
        self.wfile.write(page)

    def send_req(self, status_code, headers):
        self.send_response(status_code)
        for k, v in headers.iteritems():
            self.send_header(k, v)
        self.end_headers()
